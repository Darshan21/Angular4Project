import { Component, OnInit } from '@angular/core';
import { Response } from '@angular/http';
import { Post } from  '../posts/post.model';
import { PostService } from '../posts/post.service';
import { DataStorageService } from '../shared/data-storage.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})


export class NavbarComponent {

  brand = 'PostOffice';
  constructor(private dataStorageService: DataStorageService,private postService: PostService) {}

  onFetchData() {
    // console.log("fetch data start...");
    this.dataStorageService.getPosts()
    .subscribe(
        (posts: Post[]) => {
          console.log("subscribe....");
          this.postService.setPosts(posts);
        }
      );
    // .subscribe(
    //   (recipes: any[]) => (this.recipes = recipes,
    //     (error) => console.log(error)
    //     );
    //   )
    console.log("fetch data ends...");
  }
}


