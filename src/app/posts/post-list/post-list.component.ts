import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';

import { Post } from '../post.model';
import { PostService } from '../post.service';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.css']
})
export class PostListComponent implements OnInit, OnDestroy {
 public  posts: Post[];
  subscription: Subscription;
  public start = 0;
  public end = 10;


  constructor(private postService: PostService,
              private router: Router,
              private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.subscription = this.postService.postsChanged
      .subscribe(
        (posts: Post[]) => {
          this.posts = posts;
          console.log("recipe-list component....", this.posts);
        }
      );
    this.posts = this.postService.getPosts();
    console.log("data of getrecipes.......",this.posts);
  }

  onNewPost() {
    console.log("new city");
    this.router.navigate(['new'], {relativeTo: this.route});
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();

  }

   onClick(postEl){
     postEl++;
   }
  // loadMore(recipeEl:any){
  //   this.recipes++;
  // }


  doInfinite(): Promise<any> {
    console.log('Begin async operation');
      console.log(this.posts);
    return new Promise((resolve) => {
      setTimeout(() => {
            
        for (var i = 0; i <10 ; i++) {
          this.start  += 1;
          this.end +=1; 
          console.log(",,,,,,,,,,,,,",this.start);
          this.posts.push();
          console.log(this.posts.length);

        }

        console.log('Async operation has ended');
        resolve();
        
      },2000);
    })
  }


}
console.log("post-list component....");
