import { Component, OnInit,Input } from '@angular/core';

import { Post } from '../../../posts/post.model';

@Component({
  selector: 'app-post-item',
  templateUrl: './post-item.component.html',
  styleUrls: ['./post-item.component.css']
})

export class PostItemComponent implements OnInit  {

  @Input() post: Post;
  @Input() index: number;
  constructor() { }


  ngOnInit() {
  }


  funcLoad(){
  	console.log("app-post-item...");
  	var pagesShown:number = 5;
  	var pageSize: number = 15;
  	return pageSize * pagesShown;

  }

  function() {
  	var pagesShown:number  =1;
  	var pageSize: number  =3;
	 pagesShown = pagesShown + 1;       

	   }
}
