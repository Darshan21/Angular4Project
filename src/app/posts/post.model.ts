// import { Ingredient } from '../shared/ingredient.model';

export class Post {
  public city: string;
  public district: string;
  public id: string;
  public postId: string;
  // public ingredients: Ingredient[];

  constructor(city: string, dist: string, state: string, postId: string) {
    this.city = city;
    this.district = dist;
    this.id = state;
    this.postId = postId;
    // this.ingredients = ingredients;
  }
}
